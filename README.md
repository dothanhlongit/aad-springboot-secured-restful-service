# Securing a RESTful API using Spring Boot and Microsoft’s Azure Active Directory.

In this article it will be presented a way to create a RESTful API using Spring Boot along with Spring Boot Starters for Microsoft’s Azure Active Directory (AD).

The tutorial assumes the reader already has a subscription to Azure, hence the details about it will not be covered.
