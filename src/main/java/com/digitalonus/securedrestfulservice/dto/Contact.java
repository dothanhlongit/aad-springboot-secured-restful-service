/**
 *
 * @author Armando Montoya
 * @email armando.montoya@digitalonus.com
 * @Date Apr 24, 2020
 *
 */

package com.digitalonus.securedrestfulservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Contact {
    private final String name;
    private final String url;
    private final String email;
}
