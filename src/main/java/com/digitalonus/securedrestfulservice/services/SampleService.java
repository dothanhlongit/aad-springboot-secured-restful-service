package com.digitalonus.securedrestfulservice.services;

import com.digitalonus.securedrestfulservice.dto.Contact;
import org.springframework.stereotype.Service;

@Service
public class SampleService {

    public static final Contact DEFAULT_CONTACT = new Contact("Digital OnUs",
            "http://digitalonus.com", "gitlab@digitalonus.com");

    public Contact printDetails() {
        return DEFAULT_CONTACT;
    }
}
