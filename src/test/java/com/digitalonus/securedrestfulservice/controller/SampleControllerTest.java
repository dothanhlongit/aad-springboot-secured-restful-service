package com.digitalonus.securedrestfulservice.controller;

import com.digitalonus.securedrestfulservice.dto.Contact;
import com.digitalonus.securedrestfulservice.services.SampleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(
        value = SampleController.class

        // this disables loading up the WebSecurityConfig.java file, otherwise it fails on start up
        , useDefaultFilters = false

        // this one indicates the specific filter to be used, in this case
        // related to the SampleController we want to test
        , includeFilters = {
        @ComponentScan.Filter(
                type = FilterType.ASSIGNABLE_TYPE,
                value = SampleController.class
        )
        }
)
class SampleControllerTest {

    public static final String AUTHOR = "Armando Montoya";

    public static final String URL = "http://github.com/coder-armando";

    public static final String MAIL = "aj.montoya@outlook.com";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    SampleService sampleService;

    @BeforeEach
    void setUp() {
        // add setup stuff here
    }

    @Test
    @WithMockUser
    void testGetContactDetails() throws Exception {
        when(sampleService.printDetails())
                .thenReturn(
                        this.getContact()
                );
        mockMvc.perform(get("/samples"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(AUTHOR))
                .andExpect(jsonPath("$.url").value(URL))
                .andExpect(jsonPath("$.email").value(MAIL));

    }

    private Contact getContact() {
        return Contact
                .builder()
                .name(AUTHOR)
                .url(URL)
                .email(MAIL)
                .build();
    }

}
